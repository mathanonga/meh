# Negation
What makes a statement False if True; or, True if False.

## Simple
* statement: (Oh) $` O `$
* negation (nuh uh) $` \neg O`$

## With Modifiers

* statement:
(pure, lahat pasado) $`\forall x \in X \ni O(x)`$
* negation:
(peru mayroong di pasado)  $`\exist x \in X \ni \neg O(x)`$
